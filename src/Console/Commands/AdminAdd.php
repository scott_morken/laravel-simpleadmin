<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/15/16
 * Time: 10:34 AM
 */

namespace Smorken\SimpleAdmin\Console\Commands;

use Illuminate\Console\Command;
use Smorken\SimpleAdmin\Contracts\Services\SaveService;

class AdminAdd extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a user to the admin list';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simpleadmin:add {user_id : User ID}';

    public function __construct(protected SaveService $saveService)
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $user_id = $this->argument('user_id');
        $m = $this->saveService->saveById($user_id);
        if ($m->result) {
            $this->info("Added [{$m->model->id}] to the admin list.");
        } else {
            $this->error("Could not add $user_id to the admin list.");
        }

        return 0;
    }
}
