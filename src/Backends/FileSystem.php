<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 4/14/16
 * Time: 4:44 PM
 */

namespace Smorken\SimpleAdmin\Backends;

use Illuminate\Support\Collection;
use Smorken\SimpleAdmin\Contracts\Backend;
use Smorken\SimpleAdmin\Contracts\Model;

class FileSystem implements Backend
{
    protected ?Collection $data = null;

    public function __construct(protected string $file) {}

    public function load(): Collection
    {
        if (is_null($this->data)) {
            $this->createIfNotExists($this->file);
            $data = $this->getData($this->file);
            if (! $data instanceof Collection) {
                $data = new Collection;
            }
            $this->data = $data->keyBy('id');
        }

        return $this->data;
    }

    public function store(Model $model): bool
    {
        $data = $this->load();
        $data->put($model->id, $model);

        return $this->writeData($this->file, $data);
    }

    public function storeMulti(Collection $models, bool $truncate = false): bool
    {
        $data = $models->keyBy('id');
        if (! $truncate) {
            $data = $this->load()->merge($models->keyBy('id'));
        }
        $stored = $this->writeData($this->file, $data);
        if ($stored) {
            $this->data = $data;
        }

        return $stored;
    }

    /**
     * @throws \Smorken\SimpleAdmin\Backends\FileSystemException
     */
    protected function createIfNotExists(string $file): void
    {
        if (! $this->fileExists($file)) {
            if (! $this->putData($file, new Collection)) {
                throw new FileSystemException('Unable to create new file. Check the filesystem permissions.');
            }
        }
        if (! $this->fileIsWritable($file)) {
            throw new FileSystemException('File is not writable. Check the filesystem permissions.');
        }
    }

    protected function fileExists(string $file): bool
    {
        return file_exists($file);
    }

    protected function fileIsWritable(string $file): bool
    {
        return is_writable($file);
    }

    protected function getData(string $file): ?Collection
    {
        return unserialize(file_get_contents($file));
    }

    protected function putData(string $file, Collection $data): bool
    {
        return file_put_contents($file, serialize($data));
    }

    protected function writeData(string $file, Collection $data): bool
    {
        $this->createIfNotExists($file);
        if (! $this->putData($file, $data)) {
            throw new FileSystemException('Unable to write data to backend filesystem.');
        }
        $this->data = $data;

        return true;
    }
}
