<?php

namespace Smorken\SimpleAdmin\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property bool $result
 * @property string|null $id
 *
 * @phpstan-require-extends \Smorken\SimpleAdmin\Services\DeleteResult
 */
interface DeleteResult extends VOResult {}
