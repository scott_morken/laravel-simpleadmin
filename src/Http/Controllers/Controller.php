<?php

namespace Smorken\SimpleAdmin\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\SimpleAdmin\Contracts\Services\IndexService;
use Smorken\SimpleAdmin\Contracts\Services\SaveService;

class Controller extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'simpleadmin::admin';

    public function __construct(protected IndexService $indexService, protected SaveService $saveService)
    {
        parent::__construct();
    }

    public function doIndex(Request $request): RedirectResponse
    {
        $result = $this->saveService->saveByRequest($request);
        if ($result->result) {
            // @phpstan-ignore method.notFound
            $request->session()
                ->flash('flash:success', 'Saved admins.');
        } else {
            // @phpstan-ignore method.notFound
            $request->session()
                ->flash('flash:danger', 'Unable to save admins.');
        }

        return (new RedirectActionResult($this->actionArray('index')))->redirect();
    }

    public function index(Request $request): \Illuminate\Contracts\View\View
    {
        $result = $this->indexService->getByRequest($request);

        return $this->makeView($this->getViewName('index'))
            ->with('admins', $result->models); // @phpstan-ignore property.notFound
    }
}
