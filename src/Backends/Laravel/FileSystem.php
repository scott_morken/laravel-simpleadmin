<?php

namespace Smorken\SimpleAdmin\Backends\Laravel;

use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Collection;

class FileSystem extends \Smorken\SimpleAdmin\Backends\FileSystem
{
    protected \Illuminate\Contracts\Filesystem\Filesystem $filesystem;

    public function __construct(string $file, Factory $factory, ?string $disk = null)
    {
        $this->filesystem = $factory->disk($disk);
        parent::__construct($file);
    }

    protected function fileExists(string $file): bool
    {
        return $this->filesystem->exists($file);
    }

    protected function fileIsWritable(string $file): bool
    {
        return true;
    }

    protected function getData(string $file): ?Collection
    {
        return unserialize($this->filesystem->get($file));
    }

    protected function putData(string $file, Collection $data): bool
    {
        return $this->filesystem->put($file, serialize($data));
    }
}
