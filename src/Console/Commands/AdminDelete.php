<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/15/16
 * Time: 10:34 AM
 */

namespace Smorken\SimpleAdmin\Console\Commands;

use Illuminate\Console\Command;
use Smorken\SimpleAdmin\Contracts\Services\DeleteService;

class AdminDelete extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes a user/removes all users';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simpleadmin:delete {user_id? : User ID} --all';

    public function __construct(protected DeleteService $deleteService)
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $user_id = $this->argument('user_id');
        if ($user_id) {
            $result = $this->deleteService->deleteById($user_id);
            if ($result->result) {
                $this->info("Deleted [$user_id] from the admin list.");
            } else {
                $this->error("Could not delete $user_id from the admin list.");
            }
        } else {
            if ($this->option('all')) {
                $result = $this->deleteService->deleteAll();
                if ($result->result) {
                    $this->info('Deleted all users from the admin list.');
                } else {
                    $this->error('Could not remove users from the admin list.');
                }
            }
        }

        return 0;
    }
}
