<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 4/14/16
 * Time: 4:30 PM
 */

namespace Smorken\SimpleAdmin\Contracts;

use Illuminate\Support\Collection;

interface Storage
{
    public function all(): Collection;

    public function delete(array|string|null $id = null): int;

    public function find(string $id): ?Model;

    public function getModel(): Model;

    /**
     * Gets the sanitizer type - defaults to string
     */
    public function getTypeOf(string $column): string;

    public function isAdmin(string $id): bool;

    public function save(array $attributes): ?Model;

    public function saveMany(array $items): bool;

    public function setModel(Model $model): void;

    /**
     * Sets the type of sanitizer to use for $column
     */
    public function setTypeOf(string $column, string $type = 'int'): void;

    public function validateAndSanitize(Model $model): ?Model;
}
