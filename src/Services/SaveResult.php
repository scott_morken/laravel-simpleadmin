<?php

namespace Smorken\SimpleAdmin\Services;

use Smorken\Service\Services\VO\VOResult;
use Smorken\SimpleAdmin\Contracts\Model;

class SaveResult extends VOResult implements \Smorken\SimpleAdmin\Contracts\Services\SaveResult
{
    public function __construct(public bool $result, public ?Model $model = null) {}
}
