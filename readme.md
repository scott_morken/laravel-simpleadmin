## Simple Admin package for Laravel 6+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

Adding smorken/simpleadmin to your composer.json should auto-register
the service provider.  If not:

Add as a service provider to your config/app.php

    ...
    'providers' => array(
            \Smorken\SimpleAdmin\ServiceProvider::class,
    ...
    
Use by DI (either injected or by calling app)

```

public function __construct(\Smorken\SimpleAdmin\Contracts\Storage $provider) { }

public function someFunction($some_var)
{
    $ap = app(\Smorken\SimpleAdmin\Contracts\Storage::class);
    //find
    $model = $ap->find(1);
    //is admin?
    $is_admin = $ap->isAdmin(1);
    //save
    $model = $ap->save(['id' => 1, 'is_admin' => true]);
    //save multi
    $saved = $ap->saveMulti([
        ['id' => 1],
        ['id' => 2, 'is_admin' => true],
    ]);
    //delete all
    $deleted = $ap->delete();
    //delete one
    $deleted = $ap->delete(123);
    //delete multiple
    $deleted = $ap->delete([123, 444]);
    //all results
    $collectoin = $ap->all();
}
```

Without DI/Laravel

```
$options = [
               'default' => 'standard',
               'sanitizers' => [
                   'standard' => \Smorken\Sanitizer\Actors\Standard::class,
               ],
           ];
$sanitize = new \Smorken\Sanitizer\Sanitize($options);
$model = new \Smorken\SimpleAdmin\Models\VO();
$backend = new \Smorken\SimpleAdmin\Backends\FileSystem(__DIR__ . '/my_store.obj');
$provider = new \Smorken\SimpleAdmin\Storage\File($model, $backend, $sanitize);
```
