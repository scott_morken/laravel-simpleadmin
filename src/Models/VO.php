<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 4/14/16
 * Time: 4:34 PM
 */

namespace Smorken\SimpleAdmin\Models;

use Smorken\SimpleAdmin\Contracts\Model;

class VO implements Model
{
    public string $id;

    public bool $is_admin = false;

    public function __construct(array $attributes = [])
    {
        if ($attributes) {
            $this->id = $attributes['id'] ?? null;
            $this->is_admin = $attributes['is_admin'] ?? false;
        }
    }

    public function newInstance(array $attributes = []): static
    {
        return new static($attributes);
    }
}
