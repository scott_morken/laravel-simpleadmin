<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:11 AM
 */

namespace Smorken\SimpleAdmin;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider as SP;
use Smorken\SimpleAdmin\Console\Commands\AdminAdd;
use Smorken\SimpleAdmin\Console\Commands\AdminDelete;
use Smorken\SimpleAdmin\Contracts\Services\DeleteService;
use Smorken\SimpleAdmin\Contracts\Services\IndexService;
use Smorken\SimpleAdmin\Contracts\Services\SaveService;
use Smorken\SimpleAdmin\Contracts\Storage;

class ServiceProvider extends SP
{
    public static function defineGates(Application $app, $force = false): void
    {
        if ($force || ! $app->runningInConsole()) {
            $provider = $app[Storage::class];
            Gate::define('role-admin', fn ($user) => $provider->isAdmin($user->id));
        }
    }

    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootConfig();
        $this->bootViews();
        $this->loadRoutes();
        $this->loadCommands();
    }

    public function loadRoutes(): void
    {
        $routefile = __DIR__.'/../routes/routes.php';
        if (file_exists($routefile) && $this->app['config']->get('simpleadmin.load_routes', true)) {
            $this->loadRoutesFrom($routefile);
        }
    }

    public function register(): void
    {
        $this->bindServices();
        $this->app->bind(\Smorken\SimpleAdmin\Contracts\Storage::class, function ($app) {
            $ma = $app['config']->get('simpleadmin.model');
            $ba = $app['config']->get('simpleadmin.backend');
            $sa = $app['config']->get('simpleadmin.storage');
            $sanitize = $app[\Smorken\Sanitizer\Contracts\Sanitize::class];
            $model = $this->instantiateClassFromArray($ma);
            $backend = $this->instantiateClassFromArray($ba);

            return $this->instantiateClassFromArray([
                'impl' => $sa['impl'],
                'params' => [
                    $model,
                    $backend,
                    $sanitize,
                ],
            ]);
        });
    }

    protected function bindServices(): void
    {
        $this->app->bind(DeleteService::class, fn ($app) => new \Smorken\SimpleAdmin\Services\DeleteService($app[Storage::class]));
        $this->app->bind(IndexService::class, fn ($app) => new \Smorken\SimpleAdmin\Services\IndexService($app[Storage::class]));
        $this->app->bind(SaveService::class, fn ($app) => new \Smorken\SimpleAdmin\Services\SaveService($app[Storage::class]));
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'simpleadmin');
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('simpleadmin.php'), // @phpstan-ignore function.notFound
        ], 'config');
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'simpleadmin');
        $this->publishes([
            __DIR__.'/../views' => resource_path('/views/vendor/smorken/simpleadmin'), // @phpstan-ignore function.notFound
        ], 'views');
    }

    protected function buildParamsForClass(array $params): array
    {
        $built = [];
        foreach ($params as $v) {
            if (is_string($v) && str_starts_with($v, 'bound:')) {
                $built[] = $this->app[substr($v, 6)];
            } else {
                $built[] = $v;
            }
        }

        return $built;
    }

    protected function instantiateClassFromArray(array $data): ?object
    {
        if (isset($data['impl'])) {
            $reflect = new \ReflectionClass($data['impl']);
            $params = $this->buildParamsForClass($data['params'] ?? []);

            return $reflect->newInstanceArgs($params);
        }

        return null;
    }

    protected function loadCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                AdminAdd::class,
                AdminDelete::class,
            ]);
        }
    }
}
