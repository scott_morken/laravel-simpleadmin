<?php

namespace Tests\Smorken\SimpleAdmin\Unit\Services;

use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\SimpleAdmin\Contracts\Services\SaveResult;
use Smorken\SimpleAdmin\Contracts\Services\SaveService;
use Smorken\SimpleAdmin\Contracts\Storage;
use Smorken\SimpleAdmin\Models\VO;

class SaveServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testSaveByIdFails(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('save')
            ->once()
            ->with(['id' => '123', 'is_admin' => true])
            ->andReturnNull();
        $result = $sut->saveById('123');
        $this->assertInstanceOf(SaveResult::class, $result);
        $this->assertFalse($result->result);
        $this->assertNull($result->model);
    }

    public function testSaveByIdSucceeds(): void
    {
        $sut = $this->getSut();
        $model = new VO(['id' => '123', 'is_admin' => true]);
        $sut->getProvider()->shouldReceive('save')
            ->once()
            ->with(['id' => '123', 'is_admin' => true])
            ->andReturn($model);
        $result = $sut->saveById('123');
        $this->assertInstanceOf(SaveResult::class, $result);
        $this->assertTrue($result->result);
        $this->assertSame($model, $result->model);
    }

    public function testSaveByRequest(): void
    {
        $sut = $this->getSut();
        $admins = '111'.PHP_EOL.'222'.PHP_EOL.'';
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with();
        $sut->getProvider()->shouldReceive('saveMany')
            ->once()
            ->with(m::type('array'))
            ->andReturnUsing(function (array $users) {
                $expected = [
                    [
                        'id' => '111',
                        'is_admin' => true,
                    ],
                    [
                        'id' => '222',
                        'is_admin' => true,
                    ],
                ];
                $this->assertEquals($expected, $users);

                return true;
            });
        $result = $sut->saveByRequest(new Request([], ['admins' => $admins]));
        $this->assertInstanceOf(SaveResult::class, $result);
        $this->assertTrue($result->result);
    }

    protected function getSut(): SaveService
    {
        return new \Smorken\SimpleAdmin\Services\SaveService(m::mock(Storage::class));
    }
}
