<?php

namespace Smorken\SimpleAdmin\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\SimpleAdmin\Contracts\Storage;

interface SaveService extends BaseService
{
    public function getProvider(): Storage;

    public function saveById(string $id): SaveResult;

    public function saveByRequest(Request $request): SaveResult;
}
