<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => Config::get('simpleadmin.prefix', 'admin'),
    'middleware' => Config::get('simpleadmin.middleware', ['web', 'auth', 'can:role-admin']),
], function () {
    Route::group([
        'prefix' => 'simpleadmin',
    ], function () {
        $controller = Config::get('simpleadmin.controller', \Smorken\SimpleAdmin\Http\Controllers\Controller::class);
        Route::get('/', [$controller, 'index']);
        Route::post('/', [$controller, 'doIndex']);
    });
});
