<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/15/16
 * Time: 9:38 AM
 */

namespace Tests\Smorken\SimpleAdmin\Unit\Model;

class VOTest extends \PHPUnit\Framework\TestCase
{
    public function testNewInstanceReturnsNewModel(): void
    {
        $sut = new \Smorken\SimpleAdmin\Models\VO;
        $this->assertNotSame($sut->newInstance(), $sut);
        $this->assertInstanceOf('Smorken\SimpleAdmin\Contracts\Model', $sut->newInstance());
    }

    public function testAttributesSetProperties(): void
    {
        $attrs = ['id' => 1, 'is_admin' => true];
        $sut = new \Smorken\SimpleAdmin\Models\VO($attrs);
        $this->assertEquals(1, $sut->id);
        $this->assertTrue($sut->is_admin);
    }
}
