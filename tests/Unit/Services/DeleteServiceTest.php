<?php

namespace Tests\Smorken\SimpleAdmin\Unit\Services;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\SimpleAdmin\Contracts\Services\DeleteResult;
use Smorken\SimpleAdmin\Contracts\Services\DeleteService;
use Smorken\SimpleAdmin\Contracts\Storage;

class DeleteServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testDeleteAllWithoutCount(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with()
            ->andReturn(0);
        $result = $sut->deleteAll();
        $this->assertInstanceOf(DeleteResult::class, $result);
        $this->assertFalse($result->result);
    }

    public function testDeleteAllWithCount(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with()
            ->andReturn(5);
        $result = $sut->deleteAll();
        $this->assertInstanceOf(DeleteResult::class, $result);
        $this->assertTrue($result->result);
    }

    public function testDeleteByIdWithoutCount(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with('111')
            ->andReturn(0);
        $result = $sut->deleteById(111);
        $this->assertInstanceOf(DeleteResult::class, $result);
        $this->assertFalse($result->result);
    }

    public function testDeleteByIdWithCount(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with('111')
            ->andReturn(1);
        $result = $sut->deleteById('111');
        $this->assertInstanceOf(DeleteResult::class, $result);
        $this->assertTrue($result->result);
    }

    protected function getSut(): DeleteService
    {
        return new \Smorken\SimpleAdmin\Services\DeleteService(m::mock(Storage::class));
    }
}
