<?php

namespace Tests\Smorken\SimpleAdmin\Functional\Storage;

use Smorken\Sanitizer\Actors\Standard;
use Smorken\Sanitizer\Sanitize;
use Smorken\SimpleAdmin\Contracts\Storage;

class FileFunctionalTest extends \PHPUnit\Framework\TestCase
{
    protected string $file;

    /**
     * @var \Smorken\SimpleAdmin\Storage\File
     */
    protected Storage $sut;

    public function setUp(): void
    {
        $this->file = __DIR__.'/store.obj';
        $options = [
            'default' => 'standard',
            'sanitizers' => [
                'standard' => Standard::class,
            ],
        ];
        $m = new \Smorken\SimpleAdmin\Models\VO;
        $b = new \Smorken\SimpleAdmin\Backends\FileSystem($this->file);
        $s = new Sanitize($options);
        $this->sut = new \Smorken\SimpleAdmin\Storage\File($m, $b, $s);
        $this->sut->delete();
    }

    public function testDeleteAll(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
            ]
        );
        $this->assertEquals(2, $this->sut->delete());
        $this->assertCount(0, $this->sut->all());
    }

    public function testDeleteMultiple(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ]
        );
        $this->assertEquals(2, $this->sut->delete([1, 2]));
        $this->assertCount(1, $this->sut->all());
        $this->assertTrue($this->sut->all()->has(3));
    }

    public function testDeleteOne(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
            ]
        );
        $this->assertEquals(1, $this->sut->delete(1));
        $this->assertCount(1, $this->sut->all());
        $this->assertTrue($this->sut->all()->has(2));
    }

    public function testFind(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ]
        );
        $m = $this->sut->find(1);
        $this->assertEquals(1, $m->id);
        $this->assertEquals(false, $m->is_admin);
    }

    public function testFindIsNull(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ]
        );
        $m = $this->sut->find(4);
        $this->assertNull($m);
    }

    public function testIsAdmin(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2, 'is_admin' => true],
                ['id' => 3],
            ]
        );
        $this->assertTrue($this->sut->isAdmin(2));
    }

    public function testIsAdminIsFalse(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ]
        );
        $this->assertFalse($this->sut->isAdmin(1));
    }

    public function testSave(): void
    {
        $m = $this->sut->save(['id' => 1, 'is_admin' => true]);
        $this->assertInstanceOf('Smorken\SimpleAdmin\Contracts\Model', $m);
        $this->assertCount(1, $this->sut->all());
    }

    public function testSaveIsNull(): void
    {
        $m = $this->sut->save(['id' => 'foo', 'is_admin' => true]);
        $this->assertNull($m);
        $this->assertCount(0, $this->sut->all());
    }

    public function testSaveMany(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 2],
            ]
        );
        $this->assertTrue($r);
        $this->assertCount(2, $this->sut->all());
    }

    public function testSaveManySkipsInvalid(): void
    {
        $r = $this->sut->saveMany(
            [
                ['id' => 1],
                ['id' => 'foo'],
            ]
        );
        $this->assertTrue($r);
        $this->assertCount(1, $this->sut->all());
    }
}
