<?php

namespace Smorken\SimpleAdmin\Contracts\Services;

use Smorken\Service\Contracts\Services\BaseService;
use Smorken\SimpleAdmin\Contracts\Storage;

interface DeleteService extends BaseService
{
    public function deleteAll(): DeleteResult;

    public function deleteById(string $id): DeleteResult;

    public function getProvider(): Storage;
}
