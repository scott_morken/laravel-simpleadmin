<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/15/16
 * Time: 8:52 AM
 */

namespace Tests\Smorken\SimpleAdmin\Unit\Storage;

use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Sanitizer\Sanitize;
use Smorken\SimpleAdmin\Contracts\Model;
use Smorken\SimpleAdmin\Contracts\Storage;
use Smorken\SimpleAdmin\Models\VO;

class FileTest extends \PHPUnit\Framework\TestCase
{
    public function testAllReturnsCollection(): void
    {
        [$backend] = $this->getMocks();
        $coll = m::mock('Illuminate\Support\Collection');
        $backend->shouldReceive('load')
            ->once()
            ->andReturn($coll);
        $sut = $this->getSut([$backend]);
        $this->assertEquals($coll, $sut->all());
    }

    public function testDeleteMultiple(): void
    {
        [$backend] = $this->getMocks();
        $all = new Collection([
            new VO(['id' => 1, 'is_admin' => true]),
            new VO(['id' => 2, 'is_admin' => true]),
            new VO(['id' => 3, 'is_admin' => true]),
            new VO(['id' => 4, 'is_admin' => true]),
        ]);
        $backend->shouldReceive('load')
            ->once()
            ->andReturn($all);
        $backend->shouldReceive('storeMulti')
            ->once()
            ->with(m::type(Collection::class), true)
            ->andReturnUsing(function (Collection $collection, bool $truncate) use ($all) {
                $this->assertTrue($truncate);
                $expected = $all->slice(2)->toArray();
                $this->assertEquals($expected, $collection->toArray());

                return true;
            });
        $sut = $this->getSut([$backend]);
        $this->assertEquals(2, $sut->delete([1, 2]));
    }

    public function testDeleteNoParams(): void
    {
        [$backend] = $this->getMocks();
        $all = new Collection([
            new VO(['id' => 1, 'is_admin' => true]),
            new VO(['id' => 2, 'is_admin' => true]),
            new VO(['id' => 3, 'is_admin' => true]),
            new VO(['id' => 4, 'is_admin' => true]),
        ]);
        $backend->shouldReceive('load')
            ->once()
            ->andReturn($all);
        $backend->shouldReceive('storeMulti')
            ->once()
            ->with(m::type(Collection::class), true)
            ->andReturnUsing(function (Collection $collection, bool $truncate) {
                $this->assertTrue($truncate);
                $this->assertEmpty($collection);

                return true;
            });
        $sut = $this->getSut([$backend]);
        $this->assertEquals(4, $sut->delete());
    }

    public function testDeleteSingleId(): void
    {
        [$backend] = $this->getMocks();
        $all = new Collection([
            new VO(['id' => 1, 'is_admin' => true]),
            new VO(['id' => 2, 'is_admin' => true]),
            new VO(['id' => 3, 'is_admin' => true]),
            new VO(['id' => 4, 'is_admin' => true]),
        ]);
        $backend->shouldReceive('load')
            ->once()
            ->andReturn($all);
        $backend->shouldReceive('storeMulti')
            ->once()
            ->with(m::type(Collection::class), true)
            ->andReturnUsing(function (Collection $collection, bool $truncate) use ($all) {
                $this->assertTrue($truncate);
                $expected = $all->slice(1)->toArray();
                $this->assertEquals($expected, $collection->toArray());

                return true;
            });
        $sut = $this->getSut([$backend]);
        $this->assertEquals(1, $sut->delete(1));
    }

    public function testFindReturnsModel(): void
    {
        [$backend] = $this->getMocks();
        $all = new Collection([
            new VO(['id' => 1, 'is_admin' => true]),
            new VO(['id' => 2, 'is_admin' => true]),
            new VO(['id' => 3, 'is_admin' => true]),
            new VO(['id' => 4, 'is_admin' => true]),
        ]);
        $backend->shouldReceive('load')
            ->once()
            ->andReturn($all);
        $sut = $this->getSut([$backend]);
        $this->assertEquals($all[0], $sut->find(1));
    }

    public function testIsAdmin(): void
    {
        [$backend] = $this->getMocks();
        $all = new Collection([
            new VO(['id' => 1, 'is_admin' => true]),
            new VO(['id' => 2, 'is_admin' => true]),
            new VO(['id' => 3, 'is_admin' => true]),
            new VO(['id' => 4, 'is_admin' => true]),
        ]);
        $backend->shouldReceive('load')
            ->once()
            ->andReturn($all);
        $sut = $this->getSut([$backend]);
        $this->assertTrue($sut->isAdmin(1));
    }

    public function testSave(): void
    {
        $attrs = ['id' => '123', 'is_admin' => true];
        [$backend] = $this->getMocks();
        $backend->shouldReceive('store')
            ->once()
            ->with(m::type(Model::class))
            ->andReturnTrue();
        $sut = $this->getSut([$backend]);
        $model = $sut->save($attrs);
        $this->assertEquals(123, $model->id);
        $this->assertTrue($model->is_admin);
    }

    public function testSaveModelIsNullBySanitization(): void
    {
        $attrs = ['id' => 'foo', 'is_admin' => true];
        [$backend] = $this->getMocks();
        $backend->shouldReceive('store')
            ->never();
        $sut = $this->getSut([$backend]);
        $model = $sut->save($attrs);
        $this->assertNull($model);
    }

    public function testSaveMany(): void
    {
        $items = [
            ['id' => '111', 'is_admin' => true],
            ['id' => '222', 'is_admin' => true],
        ];
        [$backend] = $this->getMocks();
        $backend->shouldReceive('storeMulti')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $collection) use ($items) {
                $this->assertCount(2, $collection);
                foreach ($items as $i => $item) {
                    $citem = $collection[$item['id']];
                    $this->assertEquals($item['id'], $citem->id);
                    $this->assertEquals($item['is_admin'], $citem->is_admin);
                }

                return true;
            });
        $sut = $this->getSut([$backend]);
        $this->assertTrue($sut->saveMany($items));
    }

    public function testSaveManySkipsItemWhenIdFailsSanitization(): void
    {
        $items = [
            ['id' => '111', 'is_admin' => true],
            ['id' => 'bar', 'is_admin' => true],
        ];
        [$backend] = $this->getMocks();
        $backend->shouldReceive('storeMulti')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $collection) use ($items) {
                $this->assertCount(1, $collection);
                $this->assertEquals($items[0]['id'], $collection[$items[0]['id']]->id);

                return true;
            });
        $sut = $this->getSut([$backend]);
        $this->assertTrue($sut->saveMany($items));
    }

    protected function getMocks(): array
    {
        $backend = m::mock('Smorken\SimpleAdmin\Contracts\Backend');

        return [$backend];
    }

    /**
     * @return \Smorken\SimpleAdmin\Storage\File
     */
    protected function getSut($mocks): Storage
    {
        [$b] = $mocks;
        $s = new Sanitize([
            'default' => 'standard',
            'sanitizers' => [
                'standard' => \Smorken\Sanitizer\Actors\Standard::class,
                'sis' => \Smorken\Sanitizer\Actors\RdsCds::class,
                'xss' => \Smorken\Sanitizer\Actors\Xss::class,
            ],
        ]);

        return new \Smorken\SimpleAdmin\Storage\File(new VO, $b, $s);
    }
}
