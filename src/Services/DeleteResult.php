<?php

namespace Smorken\SimpleAdmin\Services;

use Smorken\Service\Services\VO\VOResult;

class DeleteResult extends VOResult implements \Smorken\SimpleAdmin\Contracts\Services\DeleteResult
{
    public function __construct(public bool $result, public ?string $id = null) {}
}
