<?php

namespace Smorken\SimpleAdmin\Services;

use Smorken\Service\Services\BaseService;
use Smorken\SimpleAdmin\Contracts\Services\DeleteResult;
use Smorken\SimpleAdmin\Contracts\Storage;

class DeleteService extends BaseService implements \Smorken\SimpleAdmin\Contracts\Services\DeleteService
{
    protected string $voClass = \Smorken\SimpleAdmin\Services\DeleteResult::class;

    public function __construct(protected Storage $provider) {}

    public function deleteAll(): DeleteResult
    {
        $count = $this->getProvider()->delete();

        /** @var DeleteResult */
        return $this->newVO(['result' => $count > 0]);
    }

    public function deleteById(string $id): DeleteResult
    {
        $count = $this->getProvider()->delete($id);

        /** @var DeleteResult */
        return $this->newVO(['result' => $count > 0, 'id' => $id]);
    }

    public function getProvider(): Storage
    {
        return $this->provider;
    }
}
