<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 4/14/16
 * Time: 4:32 PM
 */

namespace Smorken\SimpleAdmin\Contracts;

/**
 * Interface Model
 *
 *
 * @property string $id
 * @property bool $is_admin
 *
 * @phpstan-require-extends \Smorken\SimpleAdmin\Models\VO
 */
interface Model
{
    public function __construct(array $attributes = []);

    public function newInstance(array $attributes = []): static;
}
