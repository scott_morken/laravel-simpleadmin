<?php

namespace Smorken\SimpleAdmin\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Smorken\SimpleAdmin\Contracts\Model|null $model
 * @property bool $result
 *
 * @phpstan-require-extends \Smorken\SimpleAdmin\Services\SaveResult
 */
interface SaveResult extends VOResult {}
