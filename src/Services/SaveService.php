<?php

namespace Smorken\SimpleAdmin\Services;

use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;
use Smorken\SimpleAdmin\Contracts\Services\SaveResult;
use Smorken\SimpleAdmin\Contracts\Storage;

class SaveService extends BaseService implements \Smorken\SimpleAdmin\Contracts\Services\SaveService
{
    protected string $voClass = \Smorken\SimpleAdmin\Services\SaveResult::class;

    public function __construct(protected Storage $provider) {}

    public function getProvider(): Storage
    {
        return $this->provider;
    }

    public function saveById(string $id): SaveResult
    {
        $model = $this->getProvider()->save(['id' => $id, 'is_admin' => true]);

        /** @var SaveResult */
        return $this->newVO(['result' => (bool) $model, 'model' => $model]);
    }

    public function saveByRequest(Request $request): SaveResult
    {
        $data = array_map(fn ($v) => ['id' => $v, 'is_admin' => true], array_filter(explode(PHP_EOL, (string) $request->get('admins'))));
        $this->getProvider()->delete();
        $result = $this->getProvider()->saveMany($data);

        /** @var SaveResult */
        return $this->newVO(['result' => $result]);
    }
}
