<?php

namespace Tests\Smorken\SimpleAdmin\Unit\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\SimpleAdmin\Contracts\Services\IndexService;
use Smorken\SimpleAdmin\Contracts\Storage;

class IndexServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testGetByRequest(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('all')
            ->once()
            ->andReturn(new Collection);
        $result = $sut->getByRequest(new Request);
        $this->assertInstanceOf(IndexResult::class, $result);
        $this->assertInstanceOf(Collection::class, $result->models);
    }

    protected function getSut(): IndexService
    {
        return new \Smorken\SimpleAdmin\Services\IndexService(m::mock(Storage::class));
    }
}
