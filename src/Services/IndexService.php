<?php

namespace Smorken\SimpleAdmin\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Service\Services\BaseService;
use Smorken\SimpleAdmin\Contracts\Storage;

class IndexService extends BaseService implements \Smorken\SimpleAdmin\Contracts\Services\IndexService
{
    protected string $voClass = \Smorken\Service\Services\VO\IndexResult::class;

    public function __construct(protected Storage $provider) {}

    public function getByRequest(Request $request): IndexResult
    {
        $models = $this->getProvider()->all();

        /** @var IndexResult */
        return $this->newVO(['models' => $models]);
    }

    public function getProvider(): Storage
    {
        return $this->provider;
    }
}
