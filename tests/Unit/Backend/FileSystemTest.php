<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/15/16
 * Time: 8:32 AM
 */

namespace Smorken\SimpleAdmin\Backends {

    use Illuminate\Support\Collection;
    use Smorken\SimpleAdmin\Models\VO;

    function file_put_contents($path, $data)
    {
        return true;
    }

    function file_get_contents($path)
    {
        return serialize(new Collection([
            new VO(['id' => 'foo']),
            new VO(['id' => 'bar']),
        ]));
    }

    function touch($path)
    {
        return true;
    }

    function file_exists($path)
    {
        return false;
    }

    function is_writable($file)
    {
        return true;
    }
}

namespace Tests\Smorken\SimpleAdmin\Unit\Backend {

    use Illuminate\Support\Collection;
    use PHPUnit\Framework\TestCase;
    use Smorken\SimpleAdmin\Backends\FileSystem;
    use Smorken\SimpleAdmin\Contracts\Backend;
    use Smorken\SimpleAdmin\Models\VO;

    class FileSystemTest extends TestCase
    {
        /**
         * @var \Smorken\SimpleAdmin\Backends\FileSystem
         */
        protected Backend $sut;

        public function setUp(): void
        {
            parent::setUp();
            $this->sut = new FileSystem('some_file');
        }

        public function testLoad()
        {
            $expected = new Collection([
                'foo' => new VO(['id' => 'foo']),
                'bar' => new VO(['id' => 'bar']),
            ]);

            $data = $this->sut->load();
            $this->assertEquals($expected, $data);
        }

        public function testStore()
        {
            $m = new VO(['id' => 'foobar']);
            $this->sut->store($m);
            $data = $this->sut->load();
            $this->assertTrue($data->has('foobar'));
        }

        public function testStoreMultiNoTruncate()
        {
            $ms = new Collection([
                new VO(['id' => 'foobar']),
                new VO(['id' => 'fizbaz']),
            ]);
            $this->sut->storeMulti($ms);
            $data = $this->sut->load();
            $this->assertCount(4, $data);
            $this->assertTrue($data->has('foobar'));
        }

        public function testStoreMultiTruncate()
        {
            $ms = new Collection([
                new VO(['id' => 'foobar']),
                new VO(['id' => 'fizbaz']),
            ]);
            $this->sut->storeMulti($ms, true);
            $data = $this->sut->load();
            $this->assertCount(2, $data);
            $this->assertTrue($data->has('foobar'));
        }
    }
}
