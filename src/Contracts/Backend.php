<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 4/14/16
 * Time: 4:43 PM
 */

namespace Smorken\SimpleAdmin\Contracts;

use Illuminate\Support\Collection;

interface Backend
{
    public function load(): Collection;

    public function store(Model $model): bool;

    public function storeMulti(Collection $models, bool $truncate = false): bool;
}
