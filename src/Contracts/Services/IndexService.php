<?php

namespace Smorken\SimpleAdmin\Contracts\Services;

use Smorken\SimpleAdmin\Contracts\Storage;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService
{
    public function getProvider(): Storage;
}
