<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 4/14/16
 * Time: 4:38 PM
 */

namespace Smorken\SimpleAdmin\Storage;

use Illuminate\Support\Collection;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\SimpleAdmin\Contracts\Backend;
use Smorken\SimpleAdmin\Contracts\Model;
use Smorken\SimpleAdmin\Contracts\Storage;

class File implements Storage
{
    protected Model $model;

    protected array $types = [
        'id' => 'int',
        'is_admin' => 'bool',
    ];

    public function __construct(Model $model, protected Backend $backend, protected Sanitize $sanitize)
    {
        $this->setModel($model);
    }

    public function all(): Collection
    {
        return $this->backend->load();
    }

    public function delete(array|string|null $id = null): int
    {
        $count = $this->all()->count();
        if ($id === null) {
            $data = $this->collectEmpty();
        } else {
            $id = (array) $id;
            $data = $this->collectExcludingIds($id);
        }
        if ($this->executeDelete($data)) {
            return $count - $data->count();
        }

        return 0;
    }

    public function find(string $id): ?Model
    {
        $r = $this->all();

        return $this->findByIdIn($id, $r);
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    /**
     * Gets the sanitizer type - defaults to string
     */
    public function getTypeOf(string $column): string
    {
        return $this->types[$column] ?? 'string';
    }

    public function isAdmin($id): bool
    {
        $m = $this->find($id);

        return $m && $m->is_admin;
    }

    public function save(array $attributes): ?Model
    {
        $m = $this->validateAndSanitize($this->getModel()->newInstance($attributes));

        return $m ? $this->saveOne($m) : null;
    }

    public function saveMany(array $items): bool
    {
        $coll = new Collection;
        foreach ($items as $item) {
            $m = $this->validateAndSanitize($this->getModel()->newInstance($item));
            if ($m) {
                $coll->put($m->id, $m);
            }
        }

        return $this->executeSave($coll);
    }

    public function setTypeOf(string $column, string $type = 'string'): void
    {
        $this->types[$column] = $type;
    }

    public function validateAndSanitize(Model $model): ?Model
    {
        $model->id = $this->sanitize->sanitize($this->getTypeOf('id'), $model->id);
        $model->is_admin = $this->sanitize->sanitize($this->getTypeOf('is_admin'), $model->is_admin);

        return $model->id ? $model : null;
    }

    protected function collectEmpty(): Collection
    {
        return new Collection;
    }

    protected function collectExcludingIds(array $ids): Collection
    {
        return $this->all()->reject(
            fn ($item) => in_array($item->id, $ids)
        );
    }

    protected function executeDelete(Collection $data): bool
    {
        return $this->backend->storeMulti($data, true);
    }

    protected function executeSave($item): bool
    {
        if ($item instanceof Model) {
            return $this->backend->store($item);
        }
        if ($item instanceof Collection) {
            return $this->backend->storeMulti($item);
        }

        return false;
    }

    protected function findByIdIn(string $id, Collection $models): ?Model
    {
        foreach ($models as $model) {
            if ($model->id === $id) {
                return $model;
            }
        }

        return null;
    }

    protected function findByKey(string $id, Collection $models): ?Model
    {
        return $models->get($id);
    }

    protected function saveOne(Model $model): ?Model
    {
        if ($model->id && $this->executeSave($model)) {
            return $model;
        }

        return null;
    }
}
