<?php

return [
    'prefix' => 'admin',
    'middleware' => ['web', 'auth', 'can:role-admin'],
    'controller' => \Smorken\SimpleAdmin\Http\Controllers\Controller::class,
    'load_routes' => true,
    'backend' => [
        'impl' => \Smorken\SimpleAdmin\Backends\Laravel\FileSystem::class,
        'params' => [
            storage_path('admins.obj'),
            'bound:filesystem',
        ],
    ],
    'model' => [
        'impl' => \Smorken\SimpleAdmin\Models\VO::class,
        'params' => [

        ],
    ],
    'storage' => [
        'impl' => \Smorken\SimpleAdmin\Storage\File::class,
        'params' => [

        ],
    ],
];
