@extends('layouts.app')
@section('content')
    <h4>Simple Admin</h4>
    <div>Enter one ID per line.</div>
    <form method="post">
        @csrf
        <div class="form-group mb-2">
            <textarea name="admins" rows="10" maxlength="1024"
                      class="form-control">{{ implode(PHP_EOL, $admins->pluck('id')->toArray()) }}</textarea>
        </div>
        <div class="">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
