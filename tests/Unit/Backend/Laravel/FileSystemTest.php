<?php

namespace Tests\Smorken\SimpleAdmin\Unit\Backend\Laravel;

use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\SimpleAdmin\Backends\FileSystemException;
use Smorken\SimpleAdmin\Backends\Laravel\FileSystem;
use Smorken\SimpleAdmin\Contracts\Backend;
use Smorken\SimpleAdmin\Models\VO;

class FileSystemTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testLoad()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $expected = new Collection([
            'foo' => new VO(['id' => 'foo']),
            'bar' => new VO(['id' => 'bar']),
        ]);
        $serialized = serialize($expected);
        $fs->shouldReceive('exists')
            ->once()
            ->with('admin.obj')
            ->andReturn(true);
        $fs->shouldReceive('get')
            ->once()
            ->with('admin.obj')
            ->andReturn($serialized);
        $data = $sut->load();
        $this->assertEquals($expected, $data);
    }

    public function testLoadFileDoesNotExist()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $expected = new Collection;
        $serialized = serialize($expected);
        $fs->shouldReceive('exists')
            ->once()
            ->with('admin.obj')
            ->andReturn(false);
        $fs->shouldReceive('put')
            ->once()
            ->with('admin.obj', $serialized)
            ->andReturn(true);
        $fs->shouldReceive('get')
            ->once()
            ->with('admin.obj')
            ->andReturn($serialized);
        $data = $sut->load();
        $this->assertEquals($expected, $data);
    }

    public function testLoadFileDoesNotExistAndCannotWriteIsException()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $expected = new Collection;
        $serialized = serialize($expected);
        $fs->shouldReceive('exists')
            ->once()
            ->with('admin.obj')
            ->andReturn(false);
        $fs->shouldReceive('put')
            ->once()
            ->with('admin.obj', $serialized)
            ->andReturn(false);
        $this->expectException(FileSystemException::class);
        $data = $sut->load();
    }

    public function testStore()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $existing = new Collection([
            'foo' => new VO(['id' => 'foo']),
            'bar' => new VO(['id' => 'bar']),
        ]);
        $existingSerialized = serialize($existing);
        $m = new VO(['id' => 'foobar']);
        $fs->shouldReceive('exists')
            ->with('admin.obj')
            ->andReturn(true);
        $fs->shouldReceive('get')
            ->once()
            ->with('admin.obj')
            ->andReturn($existingSerialized);
        $existing->put('foobar', $m);
        $putSerialized = serialize($existing);
        $fs->shouldReceive('put')
            ->once()
            ->with('admin.obj', $putSerialized)
            ->andReturnTrue();
        $this->assertTrue($sut->store($m));
        $data = $sut->load();
        $this->assertTrue($data->has('foobar'));
    }

    public function testStoreCannotWriteIsException()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $existing = new Collection([
            'foo' => new VO(['id' => 'foo']),
            'bar' => new VO(['id' => 'bar']),
        ]);
        $existingSerialized = serialize($existing);
        $m = new VO(['id' => 'foobar']);
        $fs->shouldReceive('exists')
            ->with('admin.obj')
            ->andReturn(true);
        $fs->shouldReceive('get')
            ->once()
            ->with('admin.obj')
            ->andReturn($existingSerialized);
        $existing->put('foobar', $m);
        $putSerialized = serialize($existing);
        $fs->shouldReceive('put')
            ->once()
            ->with('admin.obj', $putSerialized)
            ->andReturnFalse();
        $this->expectException(FileSystemException::class);
        $sut->store($m);
    }

    public function testStoreMultiNoTruncate()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $ms = new Collection([
            new VO(['id' => 'foobar']),
            new VO(['id' => 'fizbaz']),
        ]);
        $existing = new Collection([
            'foo' => new VO(['id' => 'foo']),
            'bar' => new VO(['id' => 'bar']),
        ]);
        $existingSerialized = serialize($existing);
        $fs->shouldReceive('exists')
            ->with('admin.obj')
            ->andReturn(true);
        $fs->shouldReceive('get')
            ->once()
            ->with('admin.obj')
            ->andReturn($existingSerialized);
        foreach ($ms as $m) {
            $existing->put($m->id, $m);
        }
        $putSerialized = serialize($existing);
        $fs->shouldReceive('put')
            ->once()
            ->with('admin.obj', $putSerialized)
            ->andReturnTrue();
        $sut->storeMulti($ms);
        $data = $sut->load();
        $this->assertCount(4, $data);
        $this->assertTrue($data->has('foobar'));
    }

    public function testStoreMultiTruncate()
    {
        $fs = $this->getFilesystem();
        $sut = $this->getSut($fs);
        $ms = new Collection([
            'foobar' => new VO(['id' => 'foobar']),
            'fizbaz' => new VO(['id' => 'fizbaz']),
        ]);
        $fs->shouldReceive('exists')
            ->with('admin.obj')
            ->andReturn(true);
        $putSerialized = serialize($ms);
        $fs->shouldReceive('put')
            ->once()
            ->with('admin.obj', $putSerialized)
            ->andReturnTrue();
        $sut->storeMulti($ms, true);
        $data = $sut->load();
        $this->assertCount(2, $data);
        $this->assertTrue($data->has('foobar'));
    }

    protected function getFactory(\Illuminate\Contracts\Filesystem\Filesystem $fs, ?string $disk = null): Factory
    {
        $f = m::mock(Factory::class);
        $f->shouldReceive('disk')
            ->once()
            ->with($disk)
            ->andReturn($fs);

        return $f;
    }

    protected function getFilesystem(): \Illuminate\Contracts\Filesystem\Filesystem
    {
        return m::mock(\Illuminate\Contracts\Filesystem\Filesystem::class);
    }

    protected function getSut(\Illuminate\Contracts\Filesystem\Filesystem $filesystem, ?string $disk = null): Backend
    {
        $factory = $this->getFactory($filesystem, $disk);

        return new FileSystem('admin.obj', $factory);
    }
}
